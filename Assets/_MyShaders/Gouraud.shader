﻿Shader ".MyShaders/.Gouraud"
{
	Properties 
	{
	_Color ("Material Color", Color) = (1,1,1,1)
	_AmbientLight("Ambient Light", Color) = (1, 1, 1, 1)
	_ConstantAmbient("Ambient Constant", Range(0.0, 1.0)) = 0.2
	_ConstantDiffuse ("Diffuse Constant", Range(0.0, 1.0)) = 0.5
	_ConstantSpecular ("Specular Constant", Range(0.0, 1.0)) = 0.5
	}

	SubShader
	{
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float4 pos : SV_POSITION;
				float4 color  : COLOR;
			};

			fixed4 _Color;
			fixed4 _AmbientLight;
			float _ConstantAmbient;
			float _ConstantDiffuse;
			float _ConstantSpecular;

			fixed4 _LightColor0;

			float4 calculatePhongModel(float4 pos, float3 normal, fixed4 color)
			{
				float3 V = normalize(_WorldSpaceCameraPos);

				float3 L = normalize(_WorldSpaceLightPos0.xyz);

				float4 localNormal = float4(normal, 0.0);
				float3 worldNormal = mul(localNormal, _Object2World).xyz;
				float3 N = normalize(worldNormal);

				float3 R = 2 * max(0.0, dot(L, N)) * N - L;

				float3 specular = _ConstantSpecular * max(0.0, dot(R, V)) * _LightColor0.rgb * color;
				float3 diffuse  = _ConstantDiffuse * max(0.0, dot(L, N)) * _LightColor0.rgb * color;
				float3 ambient = _AmbientLight * _ConstantAmbient;

				float4 result;
				result.xyz = ambient + diffuse + specular;
				result.w = 1.0;

				return result;
			}

			v2f vert (appdata IN)
			{
				v2f OUT;

				OUT.pos = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.color = calculatePhongModel(IN.vertex, IN.normal, _Color);

				return OUT;
			}

			fixed4 frag (v2f IN) : COLOR
			{
				return IN.color;
			}

			ENDCG
		}
	}
}
